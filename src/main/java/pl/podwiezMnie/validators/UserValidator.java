package pl.podwiezMnie.validators;

import java.util.HashSet;
import java.util.Set;

import pl.podwiezMnie.entities.Role;
import pl.podwiezMnie.entities.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return arg0.isAssignableFrom(User.class);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		User u = (User) arg0;
		
		
		validateLogin(u.getLogin(), arg1);
		validatePassword(u.getPassword(), arg1);
	}

	private void validatePassword(String password, Errors errors) {
		if (StringUtils.trimWhitespace(password).length() == 0) {
			errors.rejectValue("password", "UserValidator.password.isEmpty");
		}
		
	}

	private void validateLogin(String login, Errors errors) {
		
		if (StringUtils.trimWhitespace(login).length() == 0) {
			errors.rejectValue("login", "UserValidator.login.isEmpty");
		}
		
		
	}

}
