package pl.podwiezMnie.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.podwiezMnie.entities.Car;

@Component
public class CarValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return clazz.isAssignableFrom(Car.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// TODO Auto-generated method stub
		try {
			Car car = (Car) obj;
			String brand = car.getBrand();
			double combustion = car.getCombustion();
			String model = car.getModel();
			
			validateBrand(brand,errors);
			validateCombustion(combustion,errors);
			validateModel(model,errors);
		} catch(NumberFormatException e) {
			errors.rejectValue("combustion", "CarValidator.combustion.wrongValue");
		}
		
		
	}
	
	private void validateModel(String model, Errors errors) {
		// TODO Auto-generated method stub
		if (model.trim().length() == 0) {
			errors.rejectValue("model", "CarValidator.model.isEmpty");
		}
		
	}

	private void validateCombustion(double combustion, Errors errors) {
		// TODO Auto-generated method stub		
		if (combustion <=0 ) {
			errors.rejectValue("combustion", "CarValidator.combustion.isLessOrEqualZero");
		} else {
			if (combustion >=99.9) {
				errors.rejectValue("combustion","CarValidator.combustion.outOfRange");
			}
		}
		
	}

	private void validateBrand(String brand, Errors errors) {
		if (brand.trim().length() == 0) {
			errors.rejectValue("brand", "CarValidator.brand.isEmpty");
		}
	}
	

}
