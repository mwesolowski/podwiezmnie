package pl.podwiezMnie.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.podwiezMnie.entities.Driver;
import pl.podwiezMnie.service.IDriverService;

@Component
public class DriverValidator implements Validator {

	@Autowired
	private IDriverService driverService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return clazz.isAssignableFrom(Driver.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// TODO Auto-generated method stub
		Driver driver = (Driver) obj;
		
		String email = driver.getEmail();
		String firstName = driver.getFirstName();
		String lastName = driver.getLastName();
		
		validateEmail(email, errors);
		validateFirstName(firstName, errors);
		validateLastName(lastName, errors);
	}
	
	private void validateEmail(String email, Errors errors) {
		if (email.trim().length() == 0) {
			errors.rejectValue("email", "DriverValidator.email.isEmpty");			
		} else {
			if (driverService.getDriverByEmail(email.trim()) != null) {
				errors.rejectValue("email", "DriverValidator.email.alreadyExists");
			}
		}
	}
	
	
	private void validateFirstName(String firstname, Errors errors) {
		if (firstname.trim().length() == 0) {
			errors.rejectValue("firstName", "DriverValidator.firstName.isEmpty");
		}
		
	}
	
	private void validateLastName(String lastname, Errors errors) {
		if (lastname.trim().length() == 0) {
			errors.rejectValue("lastName", "DriverValidator.lastName.isEmpty");
		}
	}
	
}
