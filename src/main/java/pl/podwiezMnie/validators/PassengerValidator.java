package pl.podwiezMnie.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import pl.podwiezMnie.entities.Passenger;
import pl.podwiezMnie.service.IPassengerService;

@Component
public class PassengerValidator implements Validator {

	@Autowired
	private IPassengerService passengerService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return clazz.isAssignableFrom(Passenger.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// TODO Auto-generated method stub
		Passenger p = (Passenger) obj;
		
		String email = p.getEmail();
		String firstName = p.getFirstName();
		String lastName = p.getLastName();
		
		validateEmail(email, errors);
		validateFirstName(firstName, errors);
		validateLastName(lastName, errors);
	}
	
	private void validateEmail(String email, Errors errors) {
		if (email.trim().length() == 0) {
			errors.rejectValue("email", "PassengerValidator.email.isEmpty");			
		} else {
			if (passengerService.getPassengerByEmail(email.trim()) != null) {
				errors.rejectValue("email", "PassengerValidator.email.alreadyExists");
			}
		}
	}
	
	
	private void validateFirstName(String firstname, Errors errors) {
		if (firstname.trim().length() == 0) {
			errors.rejectValue("firstName", "PassengerValidator.firstName.isEmpty");
		}
		
	}
	
	private void validateLastName(String lastname, Errors errors) {
		if (lastname.trim().length() == 0) {
			errors.rejectValue("lastName", "PassengerValidator.lastName.isEmpty");
		}
	}
	
}
