package pl.podwiezMnie.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries ({
	@NamedQuery(
			name = "findPassengerByEmail",
			query = "from Passenger p where upper(p.email) = upper(:email)"
			)
})

@Entity
@Table(name="passengers")
public class Passenger {

	@Id
	@Column(name="id", unique = true, nullable = false)
	@GeneratedValue
	private int id;
	
	@Column(name="imie", nullable = false)
	private String firstName;
	
	@Column(name="nazwisko", nullable = false)
	private String lastName;
	
	@Column(name="email", nullable = false, unique = true)
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
