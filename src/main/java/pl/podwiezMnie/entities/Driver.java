package pl.podwiezMnie.entities;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries ({
	@NamedQuery(
			name = "findDriverByEmail",
			query = "from Driver d where upper(d.email) = upper(:email)"
			)
})
@Entity
@Table(name="drivers")
public class Driver {

	@Id
	@Column(name="id", unique = true, nullable = false)
	@GeneratedValue
	private int id;
	
	@Column(name="imie", nullable = false)
	private String firstName;
	
	@Column(name="nazwisko", nullable = false)
	private String lastName;
	
	@Column(name="email", nullable = false, unique = true)
	private String email;
	
	@OneToMany(mappedBy="driver")
	private List<Car> cars;
	
	public List<Car> getCars() {
		return cars;
	}
	public void setCars(List<Car> list) {
		this.cars = list;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
}
