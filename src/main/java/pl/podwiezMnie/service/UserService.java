package pl.podwiezMnie.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.podwiezMnie.dao.IUserDAO;
import pl.podwiezMnie.entities.User;

@Service
public class UserService implements IUserService {

	@Autowired
	private IUserDAO userDAO;
	
	
	public IUserDAO getUserDAO() {
		return userDAO;
	}


	public void setUserDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}


	@Transactional
	public void addUser(User u) {
		userDAO.addUser(u);
		
	}


	@Transactional
	public List<User> getUserList() {
		// TODO Auto-generated method stub
		return userDAO.getUserList();
	}


	@Transactional
	public User getUserById(Integer userId) {
		// TODO Auto-generated method stub
		return userDAO.getUserById(userId);
	}

}
