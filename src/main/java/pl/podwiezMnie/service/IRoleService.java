package pl.podwiezMnie.service;
import java.util.List;
import java.util.Set;

import pl.podwiezMnie.entities.Role;


public interface IRoleService {

	public List<Role> getAllRoles();

	public Role getRoleById(int i);
}
