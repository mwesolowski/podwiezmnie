package pl.podwiezMnie.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.podwiezMnie.dao.IPassengerDAO;
import pl.podwiezMnie.entities.Passenger;

public class PassengerService implements IPassengerService {

	@Autowired
	private IPassengerDAO passengerDAO;
	
	
	
	public IPassengerDAO getPassengerDAO() {
		return passengerDAO;
	}

	public void setPassengerDAO(IPassengerDAO passengerDAO) {
		this.passengerDAO = passengerDAO;
	}

	@Transactional
	public void addPassenger(Passenger p) {
		passengerDAO.addPassenger(p);
		
	}

	@Transactional
	public void updatePassenger(Passenger p) {
		passengerDAO.updatePassenger(p);
		
	}

	@Transactional
	public List<Passenger> getAllPassengers() {
		// TODO Auto-generated method stub
		return passengerDAO.getAllPassengers();
	}

	@Transactional
	public Passenger getPassengerById(Integer passengerId) {
		// TODO Auto-generated method stub
		return passengerDAO.getPassengerById(passengerId);
	}

	@Transactional
	public void deletePassenger(Passenger p) {
		passengerDAO.deletePassenger(p);
		
	}

	@Transactional
	public Passenger getPassengerByEmail(String email) {
		// TODO Auto-generated method stub
		return passengerDAO.getPassengerByEmail(email);
	}

}
