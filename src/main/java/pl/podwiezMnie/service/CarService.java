package pl.podwiezMnie.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.podwiezMnie.dao.ICarDAO;
import pl.podwiezMnie.entities.Car;

public class CarService implements ICarService {
	
	@Autowired
	private ICarDAO carDAO;
	
	public ICarDAO getCarDAO() {
		return carDAO;
	}

	public void setCarDAO(ICarDAO carDAO) {
		this.carDAO = carDAO;
	}

	@Transactional
	public void addCar(Car car) {
		carDAO.addCar(car);
	}

	@Transactional
	public List<Car> getAllCars() {
		return carDAO.getCarList();
	}

	@Transactional
	public Car getCarById(Integer carId) {
		// TODO Auto-generated method stub
		return carDAO.getCarById(carId);
	}

	@Transactional
	public void updateCar(Car car) {
		carDAO.updateCar(car);
		
	}

	@Transactional
	public void deleteCar(Car car) {
		carDAO.deleteCar(car);
		
	}
	
	@Transactional
	public List<Car> getAllCars(Integer driverId) {
		return carDAO.getAllCars(driverId);
	}

}
