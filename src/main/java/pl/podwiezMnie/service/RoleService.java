package pl.podwiezMnie.service;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.podwiezMnie.dao.IRoleDAO;
import pl.podwiezMnie.entities.Role;

@Service
public class RoleService implements IRoleService{
	
	@Autowired
	private IRoleDAO roleDAO;

	
	public IRoleDAO getRoleDAO() {
		return roleDAO;
	}


	public void setRoleDAO(IRoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}


	@Transactional
	public List<Role> getAllRoles() {
		// TODO Auto-generated method stub
		return roleDAO.getAllRoles();
	}


	@Transactional
	public Role getRoleById(int i) {
		// TODO Auto-generated method stub
		return roleDAO.getRoleById(i);
	}

}
