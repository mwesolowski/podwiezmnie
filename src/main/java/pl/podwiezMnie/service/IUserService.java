package pl.podwiezMnie.service;

import java.util.List;

import pl.podwiezMnie.entities.User;

public interface IUserService {
	
	public void addUser(User u);

	public List<User> getUserList();

	public User getUserById(Integer userId);

}
