package pl.podwiezMnie.service;

import java.util.List;

import pl.podwiezMnie.entities.Passenger;

public interface IPassengerService {
	public void addPassenger(Passenger p);
	public void updatePassenger(Passenger p);
	public List<Passenger> getAllPassengers();
	public Passenger getPassengerById(Integer driverId);
	public void deletePassenger(Passenger p);
	public Passenger getPassengerByEmail(String email);
}
