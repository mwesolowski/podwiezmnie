package pl.podwiezMnie.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import pl.podwiezMnie.dao.IDriverDAO;
import pl.podwiezMnie.entities.Driver;

public class DriverService implements IDriverService{

	@Autowired
	private IDriverDAO driverDAO;

	public IDriverDAO getDriverDAO() {
		return driverDAO;
	}

	public void setDriverDAO(IDriverDAO driverDAO) {
		this.driverDAO = driverDAO;
	}

	@Transactional
	public void addDriver(Driver driver) {
		driverDAO.addDriver(driver);
		
	}

	@Transactional
	public void updateDriver(Driver driver) {
		driverDAO.updateDriver(driver);
		
	}

	@Transactional
	public List<Driver> getAllDrivers() {
		// TODO Auto-generated method stub
		return driverDAO.getAllDrivers();
	}

	@Transactional
	public Driver getDriverById(Integer driverId) {
		// TODO Auto-generated method stub
		return driverDAO.getDriverById(driverId);
	}

	@Transactional
	public void deleteDriver(Driver driver) {
		driverDAO.deleteDriver(driver);
		
	}

	@Transactional
	public Driver getDriverByEmail(String email) {
		// TODO Auto-generated method stub
		return driverDAO.getDriverByEmail(email);
	}

}
