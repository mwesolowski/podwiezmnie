package pl.podwiezMnie.service;

import java.util.List;

import pl.podwiezMnie.entities.Driver;

public interface IDriverService {
	
	public void addDriver(Driver driver);
	public void updateDriver(Driver driver);
	public List<Driver> getAllDrivers();
	public Driver getDriverById(Integer driverId);
	public void deleteDriver(Driver driver);
	public Driver getDriverByEmail(String email);
}
