package pl.podwiezMnie.service;

import java.util.List;

import pl.podwiezMnie.entities.Car;

public interface ICarService {
	
	public void addCar(Car car);
	public List<Car> getAllCars();
	public Car getCarById(Integer carId);
	public void updateCar(Car car);
	public void deleteCar(Car car);
	public List<Car> getAllCars(Integer id);
}
