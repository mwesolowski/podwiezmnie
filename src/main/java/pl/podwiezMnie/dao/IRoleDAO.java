package pl.podwiezMnie.dao;

import java.util.List;
import java.util.Set;

import pl.podwiezMnie.entities.Car;
import pl.podwiezMnie.entities.Role;

public interface IRoleDAO {
	
	public List<Role> getAllRoles();

	public Role getRoleById(int i);

}
