package pl.podwiezMnie.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pl.podwiezMnie.entities.Passenger;

public class PassengerDAO implements IPassengerDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void addPassenger(Passenger p) {
		sessionFactory.getCurrentSession().save(p);
		
	}

	@Override
	public void updatePassenger(Passenger p) {
		sessionFactory.getCurrentSession().update(p);
		
	}

	@Override
	public List<Passenger> getAllPassengers() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Passenger").list();
	}

	@Override
	public Passenger getPassengerById(Integer passengerId) {
		// TODO Auto-generated method stub
		return (Passenger) sessionFactory.getCurrentSession().get(Passenger.class, passengerId);
	}

	@Override
	public void deletePassenger(Passenger p) {
		sessionFactory.getCurrentSession().delete(p);
		
	}

	@Override
	public Passenger getPassengerByEmail(String email) {
		// TODO Auto-generated method stub
		return (Passenger) sessionFactory.getCurrentSession().getNamedQuery("findPassengerByEmail").setString("email", email).uniqueResult();
	}

}
