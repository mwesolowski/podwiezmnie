package pl.podwiezMnie.dao;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pl.podwiezMnie.dao.IRoleDAO;
import pl.podwiezMnie.entities.Role;


public class RoleDAO implements IRoleDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	public List<Role> getAllRoles() {
		return sessionFactory.getCurrentSession().createQuery("from Role").list();
	}


	public Role getRoleById(int i) {
		return (Role) sessionFactory.getCurrentSession().get(Role.class, i);
	}

}
