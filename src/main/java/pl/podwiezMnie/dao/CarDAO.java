package pl.podwiezMnie.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pl.podwiezMnie.entities.Car;

public class CarDAO implements ICarDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void addCar(Car car) {
		sessionFactory.getCurrentSession().save(car);
	}

	public List<Car> getCarList() {
		return getSessionFactory().getCurrentSession().createQuery("from Car").list();
	}

	public Car getCarById(Integer carId) {
		// TODO Auto-generated method stub
		Car car = new Car();
		car = (Car) getSessionFactory().getCurrentSession().get(Car.class, carId);
		
		return car;
	}

	public void updateCar(Car car) {
		sessionFactory.getCurrentSession().update(car);
		
	}

	
	public void deleteCar(Car car) {
		sessionFactory.getCurrentSession().delete(car);
		
	}

	@Override
	public List<Car> getAllCars(Integer driverId) {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("FROM Car WHERE driver_id="+driverId).list();
	}
}
