package pl.podwiezMnie.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pl.podwiezMnie.entities.Driver;

public class DriverDAO implements IDriverDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	public void addDriver(Driver driver) {
		sessionFactory.getCurrentSession().save(driver);
		
	}


	public void updateDriver(Driver driver) {
		sessionFactory.getCurrentSession().update(driver);
		
	}


	public List<Driver> getAllDrivers() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Driver").list();
	}


	public Driver getDriverById(Integer driverId) {
		// TODO Auto-generated method stub
		return (Driver) sessionFactory.getCurrentSession().get(Driver.class, driverId);
	}

	@Override
	public void deleteDriver(Driver driver) {
		sessionFactory.getCurrentSession().delete(driver);
		
	}

	@Override
	public Driver getDriverByEmail(String email) {
		// TODO Auto-generated method stub
		return (Driver) sessionFactory.getCurrentSession().getNamedQuery("findDriverByEmail").setString("email",email).uniqueResult();
	}

}
