package pl.podwiezMnie.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.podwiezMnie.entities.Role;
import pl.podwiezMnie.entities.User;

@Repository
public class UserDAO implements IUserDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addUser(User u) {
		sessionFactory.getCurrentSession().save(u);
		
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<User> getUserList() {
		List<User> uList = sessionFactory.getCurrentSession().createQuery("from User").list();
		
		// accessing the role list to make populating collection in Users entity
		for (User u: uList) {
			u.getRoles().iterator().next();
		}
			
		
		
		
		
		// TODO Auto-generated method stub
		return uList;
	}

	@Override
	public User getUserById(Integer userId) {
		// TODO Auto-generated method stub
		User u = (User) sessionFactory.getCurrentSession().get(User.class, userId);
		u.getRoles().iterator().next();
		return u;
		
	}
	
}
