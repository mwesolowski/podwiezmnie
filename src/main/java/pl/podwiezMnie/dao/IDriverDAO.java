package pl.podwiezMnie.dao;

import java.util.List;

import pl.podwiezMnie.entities.Driver;

public interface IDriverDAO {

	public void addDriver(Driver driver);
	public void updateDriver(Driver driver);
	public List<Driver> getAllDrivers();
	public Driver getDriverById(Integer driverId);
	public void deleteDriver(Driver driver);
	public Driver getDriverByEmail(String email);

}
