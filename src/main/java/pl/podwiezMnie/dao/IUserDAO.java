package pl.podwiezMnie.dao;

import java.util.List;

import pl.podwiezMnie.entities.User;

public interface IUserDAO {
	
	public void addUser(User u);

	public List<User> getUserList();

	public User getUserById(Integer userId);

}
