package pl.podwiezMnie.dao;

import java.util.List;

import pl.podwiezMnie.entities.Car;

public interface ICarDAO {
	
	public void addCar(Car car);
	public List<Car> getCarList();
	public Car getCarById(Integer carId);
	public void updateCar(Car car);
	public void deleteCar(Car car);
	public List<Car> getAllCars(Integer driverId);

}
