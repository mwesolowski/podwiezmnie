package pl.podwiezMnie.web.controllers;

import java.util.ArrayList;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.podwiezMnie.entities.Driver;
import pl.podwiezMnie.entities.Passenger;
import pl.podwiezMnie.service.IPassengerService;
import pl.podwiezMnie.validators.PassengerValidator;

@Controller
@RequestMapping("/passenger")
public class PassengerController {
	
	@Autowired
	private IPassengerService passengerService;
	
	@Autowired
	private PassengerValidator passengerValidator;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(passengerValidator);
	}
	private Passenger stringCleanUp(Passenger p) {
		p.setFirstName(p.getFirstName().trim());
		p.setLastName(p.getLastName().trim());
		p.setEmail(p.getEmail().trim());
		
		return p;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String defaultController() {
		return "passenger/indexPassenger";
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public String viewAddPassenger(Model model) {
		model.addAttribute(new Passenger());
		return "passenger/addPassenger";
	}

	@RequestMapping(value="/add", method = RequestMethod.POST)
	public String parseAddPassenger(@Valid Passenger passenger,
			BindingResult result, ModelMap model) {
		
		if (result.hasErrors()) {
			return "passenger/addPassenger";
		}
		
		String info = "Pomyślnie dodano pasażera do bazy.";
		
		stringCleanUp(passenger);
		
		passengerService.addPassenger(passenger);
		model.put("passenger", passenger);
		model.put("info", info);
		
		return "passenger/addPassenger";
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public String viewPassengersToEdit(Map<String, Object> map) {
		map.put("passenger", new Passenger());
		map.put("passengerList", passengerService.getAllPassengers());
		
		return "passenger/editPassenger";
	}
	
	@RequestMapping(value="/edit/{passengerId}", method = RequestMethod.GET)
	public String viewPassengerEditForm(@PathVariable Integer passengerId, ModelMap model) {
		
		model.put("passenger", passengerService.getPassengerById(passengerId));
		
		return "passenger/editPassengerForm";
	}
	
	@RequestMapping(value="/edit/{passengerId}", method = RequestMethod.POST)
	public String parsePassengerEdit(@PathVariable Integer passengerId, 
			@Valid Passenger p, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "passenger/editPassengerForm";
		}
		
		String info = "Pomyślnie uaktualniono dane pasażera.";
		
		stringCleanUp(p);
		model.put("passenger", p);
		model.put("info", info);
		
		p.setId(passengerId);
		passengerService.updatePassenger(p);
		return "passenger/editPassengerForm";
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.GET)
	public String viewPassengersToDelete(Map<String, Object> map) {
		map.put("passenger", new Passenger());
		map.put("passengerList", passengerService.getAllPassengers());
		
		return "passenger/deletePassenger";
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public String parsePassengersToDelete (ModelMap model, @RequestParam(value="id", required = true,
			defaultValue="") int[] ids) {
		
		ArrayList<Passenger> passengerList = new ArrayList<Passenger>();
		
		try {
			for (int i=0; i<ids.length; i++) {
				passengerList.add(passengerService.getPassengerById(ids[i]));
				passengerService.deletePassenger(passengerList.get(i));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			model.addAttribute("name", e.getClass().getSimpleName());
			model.addAttribute("error", "Próba wysłania pustego żądania.");
			return "error";
		}
		
		model.addAttribute("passenger", new Passenger());
		model.addAttribute("passengerList", passengerList);
		
		model.addAttribute("info", "Pomyślnie usnięto pasażera z bazy.");
		return "passenger/deletePassengerResult";
	}
	
	@RequestMapping(value="/showAll", method = RequestMethod.GET)
	public String showAllPassengers(Map<String, Object> map) {
		map.put("passenger", new Passenger());
		map.put("passengerList", passengerService.getAllPassengers());
		
		return "passenger/showAllPassengers";
	}
}
