package pl.podwiezMnie.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.podwiezMnie.entities.Driver;
import pl.podwiezMnie.service.ICarService;
import pl.podwiezMnie.service.IDriverService;
import pl.podwiezMnie.validators.DriverValidator;

@Controller
@RequestMapping("/driver")

public class DriverController {
	@Autowired
	private DriverValidator driverValidator;
	
	@Autowired
	private IDriverService driverService;
	
	@Autowired
	private ICarService carService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
    	binder.setValidator(driverValidator);
    }
	
	private Driver stringCleanUp(Driver driver) {
		driver.setFirstName(driver.getFirstName().trim());
		driver.setLastName(driver.getLastName().trim());
		driver.setEmail(driver.getEmail().trim());
		
		return driver;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String defaultController() {
		return "driver/indexDriver";
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET) 
	public String viewAddDriver(Model model) {
		model.addAttribute(new Driver());
		return "driver/addDriver";
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public String parseAddDriver(@Valid Driver driver,BindingResult result, ModelMap model ) {
		
		if (result.hasErrors()) {
			return "driver/addDriver";
		}
		String info = "Pomyślnie dodano kierowcę do bazy.";
		
		stringCleanUp(driver); // usuwa spacje z pol
		model.put("driver", driver);
		model.put("info", info);
		
		
		
		driverService.addDriver(driver);
		return "driver/addDriver";
		
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public String viewDriversToEdit(Map<String, Object> map) {
		map.put("driver", new Driver());
		map.put("driverList", driverService.getAllDrivers());
		
		return "driver/editDriver";
	}
	
	@RequestMapping(value="/edit/{driverId}", method = RequestMethod.GET)
	public String viewDriverEditForm(@PathVariable Integer driverId, ModelMap model) {
		
		Driver d = driverService.getDriverById(driverId);
		d.setCars(carService.getAllCars(driverId));
		
		model.put("driver", d);
		
		return "driver/editDriverForm";
	}
	
	@RequestMapping(value="/edit/{driverId}", method = RequestMethod.POST)
	public String parseEditDriver(@PathVariable Integer driverId, @Valid Driver driver,
			BindingResult result, ModelMap model) {
		
		driver.setCars(carService.getAllCars(driverId));
		
		if (result.hasErrors()) {
			
			return "driver/editDriverForm";
		}
		String info = "Pomyślnie uaktualniono dane kierowcy.";
		
		stringCleanUp(driver);
		model.put("driver", driver);
		model.put("info", info);
		
		driver.setId(driverId);
		driverService.updateDriver(driver);
		return "driver/editDriverForm";
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.GET)
	public String viewDriversToDelete(Map<String, Object> map) {
		map.put("driver", new Driver());
		map.put("driverList", driverService.getAllDrivers());
		
		return "driver/deleteDriver";
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public String parseCarsToDelete (ModelMap model, @RequestParam(value="id", required = true,
			defaultValue="") int[] ids) {
		
		ArrayList<Driver> driverList = new ArrayList<Driver>();
		
		try {
			for (int i=0; i<ids.length; i++) {
				driverList.add(driverService.getDriverById(ids[i]));
				driverService.deleteDriver(driverList.get(i));
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			model.addAttribute("name", e.getClass().getSimpleName());
			model.addAttribute("error", "Próba wysłania pustego żądania.");
			return "error";
		}
		
		model.addAttribute("driver", new Driver());
		model.addAttribute("driverList", driverList);
		
		model.addAttribute("info", "Pomyślnie usnięto kierowcę z bazy.");
		return "driver/deleteDriverResult";
	}
	
	@RequestMapping(value="/showAll", method = RequestMethod.GET)
	public String showAllDrivers(Map<String, Object> map) {

		List<Driver> drivers = driverService.getAllDrivers();
		
		List<Driver> newDrivers = new ArrayList<Driver>();
		
		for (Driver driver: drivers) {
			Driver d = new Driver();
			
			d.setId(driver.getId());
			d.setEmail(driver.getEmail());
			d.setFirstName(driver.getFirstName());
			d.setLastName(driver.getLastName());
			d.setCars(carService.getAllCars(driver.getId()));
			
			newDrivers.add(d);
		}
		
		map.put("drivers", newDrivers);
		
		
		return "driver/showAllDrivers";
	}
	
	


}
