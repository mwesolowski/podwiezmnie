package pl.podwiezMnie.web.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.podwiezMnie.entities.Role;
import pl.podwiezMnie.entities.User;
import pl.podwiezMnie.service.IRoleService;
import pl.podwiezMnie.service.IUserService;
import pl.podwiezMnie.validators.UserValidator;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserValidator userValidator;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IRoleService roleService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(userValidator);
	}
	@RequestMapping(method = RequestMethod.GET)
	public String defaultController() {
		return "user/index";
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public String addView(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("roleList", roleService.getAllRoles());
		return "user/add";
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public String processAdd(@Valid User u, 
			BindingResult result,
			@RequestParam(value="id", required=true, defaultValue="") int[] rolesId,
			Model model
			) {
		
		if (result.hasErrors() || rolesId.length == 0) {
			if (rolesId.length == 0) {
				model.addAttribute("roleError", "Nie podano ról.");
			}
			model.addAttribute("error", true);
			model.addAttribute("roleList", roleService.getAllRoles());
			return "user/add";
		}
		
		Set<Role> r = new HashSet<Role>();
		
		for (int i=0; i<rolesId.length; i++) {
			r.add(roleService.getRoleById(rolesId[i]));
		}
		u.setRoles(r);
		userService.addUser(u);
		
		model.addAttribute("error", false);
		
		return "user/add";
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public String chooseUserEdit(Map<String, Object> map) {
		map.put("user", new User());
		map.put("userList", userService.getUserList());
		return "user/edit";
	}
	
	@RequestMapping(value="/edit/{userId}", method = RequestMethod.GET)
	public String viewUserForm(@PathVariable Integer userId, 
			Map<String, Object> map) {
		
		User u = userService.getUserById(userId);
		map.put("user", u);
		map.put("roleList", roleService.getAllRoles());		
		return "user/edit";
	}

}
