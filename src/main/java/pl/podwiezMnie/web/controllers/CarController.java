package pl.podwiezMnie.web.controllers;

import java.util.ArrayList;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionException;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.podwiezMnie.entities.Car;
import pl.podwiezMnie.service.ICarService;
import pl.podwiezMnie.validators.CarValidator;


@Controller
@RequestMapping("/car")
public class CarController {
	
		@Autowired
		private CarValidator carValidator;
		
		@Autowired
		private ICarService carService;
		
        @RequestMapping(method=RequestMethod.GET)
        public String defaultController() {
                return "car/indexCar";
        }
        @RequestMapping(value="/add", method=RequestMethod.GET)
        public String addCar(ModelMap model) {
        	Car car = new Car();
        	model.put("car", car);
            return "car/addCar";
        }
        
        @InitBinder
        protected void initBinder(WebDataBinder binder) {
        	binder.setValidator(carValidator);
        }
        
        @ExceptionHandler(TransactionException.class)
        public ModelAndView handleHibernateTransactionException(TransactionException ex) {
        	return errorModelAndView(ex);
        }
        
        private ModelAndView errorModelAndView(Exception ex) {
        	ModelAndView modelAndView = new ModelAndView();
        	
        	modelAndView.setViewName("error");
        	modelAndView.addObject("name",ex.getClass().getSimpleName());
        	modelAndView.addObject("error",(String) "Błąd połączenia z bazą danych.");
        	ex.printStackTrace();
        	return modelAndView;
        }
        
        
        // zrob walidacje jak POST formularza
        @RequestMapping(value="/add", method=RequestMethod.POST) 
        public String processAdd(@Valid Car car, BindingResult result, ModelMap model) {
        	
        	if (result.hasErrors()) {
        		return "car/addCar";
        	}
        	String info = "Pomyślnie dodano samochód do bazy.";
        	model.put("car", car);
        	model.put("info", info);
        	carService.addCar(car);
        	
        	return "car/addCar";
        }
      
    	@RequestMapping(value="/showAll", method = RequestMethod.GET) 
    	public String showAllCars(Map<String, Object> map) {
    		
    		map.put("car", new Car());
    		map.put("carList", carService.getAllCars());
    		return "car/showAllCars";
    	}
    	
    	@RequestMapping(value="/edit", method= RequestMethod.GET)
    	public String chooseCarToEdit(Map<String,Object> map) {
    		map.put("car", new Car());
    		map.put("carList", carService.getAllCars());
    		return "car/editCar";
    	}
    	
    	@RequestMapping(value="/edit/{carId}", method= RequestMethod.GET)
    	public String viewCarEditAction(@PathVariable Integer carId, ModelMap model) {
    		
    		model.put("car", carService.getCarById(carId));
    		return "car/editCarAction";
    	}
    	
    	
    	@RequestMapping(value="/edit/{carId}", method = RequestMethod.POST)
    	public String processCarEditAction(@PathVariable Integer carId, @Valid Car car, BindingResult result, ModelMap model) {
    		if (result.hasErrors()) {
    			return "car/editCarAction";
    		}
    		String info = "Pomyślnie uaktualniono dane samochodu.";
    		model.put("car", car);
    		model.put("info", info);
    		car.setId(carId);
    		carService.updateCar(car);
    		return "car/editCarAction";
    	}
    	
    	@RequestMapping(value="/delete", method = RequestMethod.GET)
    	public String showCarsToDelete(ModelMap model) {
    		model.put("car", new Car());
    		model.put("carList", carService.getAllCars());
    		return "car/deleteCar";
    	}
    	
    	
    	// TODO: handle IllegalArgumentException when trying to refresh page after deletion
    	// (resend attempt)
    	@RequestMapping(value="/delete", method=RequestMethod.POST)
    	public String postAndDeleteCars(ModelMap model, @RequestParam(value="id", required=true,
    			defaultValue="") int[] ids) {
    		
    		
    		ArrayList<Car> carList = new ArrayList<Car>();

    		try {
    		for (int i=0; i<ids.length; i++) {
    			carList.add(carService.getCarById(ids[i]));
    			carService.deleteCar(carList.get(i));
    		}
    		
    		} catch (IllegalArgumentException e) {
    			e.printStackTrace();
            	model.addAttribute("name",e.getClass().getSimpleName());
            	model.addAttribute("error","Próba wysłania pustego żądania.");
    			return "error";
    		}
    		
    		model.addAttribute("car", new Car());
    		model.addAttribute("carList", carList);
    		return "car/deleteCarAction";
    	}
    	
   
}