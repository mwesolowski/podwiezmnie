<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <title>HTML5 Hello World!</title>
</head>
<body>
        <h2>Parametry dodanego auta:</h2>
         
        Marka: ${car.brand}<br />
        Model: ${car.model}<br />
        Spalanie: ${car.combustion}<br />
        
        <a href="/PodwiezMnie">Powrót do początku</a>
</body>
</html>