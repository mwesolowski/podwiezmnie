<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Samochody w bazie</title>
</head>
<body>
	<h1 id="greeting">Lista samochodów w bazie</h1>
	<c:choose>
		<c:when test="${!empty carList }">
			<table class="data">
				<tr>
					<th>Marka</th>
					<th>Model</th>
					<th>Spalanie</th>
					<th>Przypisany kierowca</th>
				</tr>
				<c:forEach items="${carList}" var="car">
					<tr>
						<td>${car.brand }</td>
						<td>${car.model }</td>
						<td>${car.combustion }</td>
						<td></td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
			Brak samochodów w bazie.<br />
		</c:otherwise>
	</c:choose>
	
	<a href="/PodwiezMnie/">Powrót do strony głównej</a>
</body>
</html>