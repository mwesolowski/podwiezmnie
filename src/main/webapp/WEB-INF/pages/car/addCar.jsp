<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <title>Dodawanie samochodu</title>
</head>
<body>
        <h1>Dodawanie samochodu</h1>
         
        <h2>Podaj dane samochodu:</h2>
        
        <form:form commandName="car">
			<table>
			<tr>
				<td>Marka</td>
				<td><form:input path="brand" /></td>
				<td><form:errors path="brand"/></td>
			</tr>
			<tr>
				<td>Model</td>
				<td><form:input path="model" /></td>
				<td><form:errors path="model"/></td>
			</tr>
			<tr>
				<td>Spalanie</td>
				<td><form:input path="combustion" /></td>
				<td><form:errors path="combustion"/></td>
			</tr>
			<tr>
				<td><input type="submit" value="Zapisz"/>
			</tr>
			</table>                
        </form:form>
        ${info }<br />
        
        <a href="/PodwiezMnie/car">Powrót</a>
</body>
</html>