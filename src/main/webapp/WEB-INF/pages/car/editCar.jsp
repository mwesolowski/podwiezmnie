<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wybór samochodu do edycji</title>
</head>
<body>
	<h1 id="greeting">Wybierz samochód do edycji:</h1>
	<c:choose>
		<c:when test="${!empty carList }">
			<table class="data">
				<tr>
					<th>Marka</th>
					<th>Model</th>
					<th>Spalanie</th>
					<th>Akcja</th>
				</tr>
				<c:forEach items="${carList}" var="car">
					<tr>
						<td>${car.brand }</td>
						<td>${car.model }</td>
						<td>${car.combustion }</td>
						<td><a href="edit/${car.id }">Edytuj</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
			Brak samochodów w bazie.<br />
		</c:otherwise>
	</c:choose>
	<a href="/PodwiezMnie/car">Powrót</a>
</body>
</html>