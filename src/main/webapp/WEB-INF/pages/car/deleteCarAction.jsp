<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Usuwanie samochodu</title>
</head>
<body>
	<h1 id="greeting">Usunięto samochody</h1>
	
	<form:form commandName="car">
		<c:choose>
			<c:when test="${!empty carList }">
				<table class="data">
					<tr>
						<th>Marka</th>
						<th>Model</th>
						<th>Spalanie</th>
					</tr>
					<c:forEach items="${carList}" var="car">
						<tr>
							<td>${car.brand }</td>
							<td>${car.model }</td>
							<td>${car.combustion }</td>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:otherwise>
				Nie wskazano samochodów do usunięcia.<br />
			</c:otherwise>
		</c:choose>
		</form:form>
	<a href="/PodwiezMnie/car">Powrót</a>
</body>
</html>