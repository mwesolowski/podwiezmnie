<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wybór samochodu do usunięcia</title>
</head>
<body>
	<h1 id="greeting">Wybierz samochód do usunięcia:</h1>
	<form:form commandName="car">
		<c:choose>
			<c:when test="${!empty carList }">
				<table class="data">
					<tr>
						<th>Marka</th>
						<th>Model</th>
						<th>Spalanie</th>
						<th>Kierowca</th>
						<th>Usuń</th>
					</tr>
					<c:forEach items="${carList}" var="car">
						<tr>
							<td>${car.brand }</td>
							<td>${car.model }</td>
							<td>${car.combustion }</td>
							<td></td>
							<td><form:checkbox path="id" value="${car.id }" /></td>
						</tr>
					</c:forEach>
					<tr>
						<td><input type="submit" value="Wyślij"></td>
					</tr>
				</table>
			</c:when>
			<c:otherwise>
				Brak samochodów w bazie.<br />
			</c:otherwise>
		</c:choose>
		</form:form>
	<a href="/PodwiezMnie/car">Powrót</a>
</body>
</html>