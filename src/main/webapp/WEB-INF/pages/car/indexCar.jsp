<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Zarządzanie samochodami</title>
</head>
<body>
	<h1>Zarządzanie samochodami</h1>
	Wybierz akcję do wykonania:<br />
	<a href="/PodwiezMnie/car/add">Dodaj samochód</a><br />
	<a href="/PodwiezMnie/car/edit">Zmień dane samochodu</a><br />
	<a href="/PodwiezMnie/car/delete">Usuń samochód z bazy</a><br />
	<a href="/PodwiezMnie/car/showAll">Pokaż wszystkie samochody</a><br />
	<br />
	<a href="/PodwiezMnie">Powrót</a>
</body>
</html>


