<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Spring Security Demo</title>
</head>
<body>
	
	<h3>Message : ${message}</h3>	
	<h3>Username : ${username}</h3>	
	
	<h4>Rola: </h4>
 	
	<a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
	
</body>
</html>