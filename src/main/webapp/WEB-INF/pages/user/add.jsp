<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Dodawanie użytkownika</title>
</head>
<body>
	<c:if test="${(user.id == 0) || (not empty roleError) || (error == true)}">
	
	<h1 id="greeting">Dodaj użytkownika</h1>
		<form:form commandName="user">
		
		Login: <form:input path="login"/><form:errors path="login" /><br />
		Hasło: <form:password path="password"/><form:errors path="password" /><br />
		Rola: <c:forEach items="${roleList }" var="role">
			<form:checkbox path="id" value="${role.id }"/> ${role.name }
		</c:forEach>${roleError }
		
		<br />
		<input type="submit" value="Wyślij" />
		
		</form:form>
	
	</c:if>
	<c:if test="${error == false }">
		Użytkownik dodany pomyślnie. <br />
		Login: ${user.login }<br />
		Hasło: ${user.password } <br />
		Role: 
		<c:forEach items="${user.roles }" var="role">
			${role.name }
		</c:forEach>
	</c:if>
		
</body>
</html>