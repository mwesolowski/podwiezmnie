<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Edycja użytkownika</title>
</head>
<body>
	<h1 id="greeting">Wybierz użytkownika do edycji</h1>
	
	<c:choose>
		<c:when test="${!empty userList }">
			<table>
				<tr>
					<th>Login</th>
					<th>Role</th>
					<th>Akcja</th>
				</tr>
				<c:forEach items="${userList }" var="user">
				<tr>
					<td>${user.login}</td>
					<td>
					<c:forEach items="${user.roles }" var="role">
						${role.name }<br />
					</c:forEach>
					</td>
					<td>
						<a href="/PodwiezMnie/user/edit/${user.id }">Edytuj</a>
					</td>
				</tr>
				
				</c:forEach>
			</table>
		</c:when>
		<c:when test="${user.id != 0 }">
				<form:form commandName="user">
			
			Login: <form:input path="login"/><form:errors path="login" /><br />
			Hasło: <form:password path="password"/><form:errors path="password" /><br />
			Rola: 
			<c:forEach items="${roleList }" var="role">
				<c:set var="hasRole" value="false" scope="page" />
				<c:forEach items="${user.roles }" var="userRoles">
					<c:if test="${userRoles.id == role.id }">
						<c:set var="hasRole" value="true" scope="page" />
					</c:if>
				</c:forEach>
				<c:if test="${hasRole }">
					<form:checkbox path="id" value="${role.id }" checked="on"/> ${role.name }
				</c:if>
				<c:if test="${!hasRole }">
					<form:checkbox path="id" value="${role.id }"/> ${role.name }
				</c:if>
			</c:forEach>${roleError }
			
			<br />
			<input type="submit" value="Wyślij" />
			
			</form:form>
		</c:when>
	</c:choose>
</body>
</html>