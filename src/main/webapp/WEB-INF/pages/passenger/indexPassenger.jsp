<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Zarządzanie pasażerami</title>
</head>
<body>
	<h1>Zarządzanie pasażerami</h1>
	Wybierz akcję do wykonania:<br />
	<a href="/PodwiezMnie/passenger/add">Dodaj pasażera</a><br />
	<a href="/PodwiezMnie/passenger/edit">Zmień dane pasażera</a><br />
	<a href="/PodwiezMnie/passenger/delete">Usuń pasażera z bazy</a><br />
	<a href="/PodwiezMnie/passenger/showAll">Pokaż pasażerów w bazie</a><br />
	<br />
	<a href="/PodwiezMnie">Powrót</a>
</body>
</html>