<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <title>Dodawanie pasażera</title>
</head>
<body>
        <h1>Dodawanie pasażera</h1>
         
        <h2>Podaj dane pasażera:</h2>
        
        <form:form commandName="passenger">
			<table>
			<tr>
				<td>Imię</td>
				<td><form:input path="firstName" /></td>
				<td><form:errors path="firstName"/></td>
			</tr>
			<tr>
				<td>Nazwisko</td>
				<td><form:input path="lastName" /></td>
				<td><form:errors path="lastName"/></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email"/></td>
			</tr>
			<tr>
				<td><input type="submit" value="Zapisz"/>
			</tr>
			</table>                
        </form:form>
        ${info }<br />
        <a href="/PodwiezMnie/passenger">Powrót</a>
</body>
</html>