<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Usuwanie pasażera</title>
</head>
<body>
	<h1 id="greeting">Usunięto pasażera</h1>
	
	<form:form commandName="passenger">
		<c:choose>
			<c:when test="${!empty passengerList }">
				<table class="data">
					<tr>
						<th>Imię</th>
						<th>Nazwisko</th>
						<th>Email</th>
					</tr>
					<c:forEach items="${passengerList}" var="passenger">
						<tr>
							<td>${passenger.firstName }</td>
							<td>${passenger.lastName }</td>
							<td>${passenger.email }</td>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:otherwise>
				Nie wskazano pasażerów do usunięcia.<br />
			</c:otherwise>
		</c:choose>
		</form:form>
	<a href="/PodwiezMnie/passenger">Powrót</a>
</body>
</html>