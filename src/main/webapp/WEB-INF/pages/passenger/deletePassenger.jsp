<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wybór pasażera do usunięcia</title>
</head>
<body>
	<h1 id="greeting">Wybierz pasażera do usunięcia:</h1>
	<form:form commandName="passenger">
		<c:choose>
			<c:when test="${!empty passengerList }">
				<table class="data">
					<tr>
						<th>Imię</th>
						<th>Nazwisko</th>
						<th>Email</th>
						<th>Usuń</th>
					</tr>
					<c:forEach items="${passengerList}" var="passenger">
						<tr>
							<td>${passenger.firstName }</td>
							<td>${passenger.lastName }</td>
							<td>${passenger.email }</td>
							<td><form:checkbox path="id" value="${passenger.id }" /></td>
						</tr>
					</c:forEach>
					<tr>
						<td><input type="submit" value="Wyślij"></td>
					</tr>
				</table>
			</c:when>
			<c:otherwise>
				Brak pasażerów w bazie.<br />
			</c:otherwise>
		</c:choose>
		</form:form>
	<a href="/PodwiezMnie/passenger">Powrót</a>
</body>
</html>