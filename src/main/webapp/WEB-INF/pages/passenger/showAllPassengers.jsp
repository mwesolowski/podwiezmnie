<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Pasażerowie w bazie</title>
</head>
<body>
	<h1 id="greeting">Lista pasażerów w bazie</h1>
	<c:choose>
		<c:when test="${!empty passengerList }">
			<table class="data">
				<tr>
					<th>Imię</th>
					<th>Nazwisko</th>
					<th>Email</th>
				</tr>
				<c:forEach items="${passengerList}" var="passenger">
					<tr>
						<td>${passenger.firstName }</td>
						<td>${passenger.lastName }</td>
						<td>${passenger.email }</td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
			Brak pasażerów w bazie.<br />
		</c:otherwise>
	</c:choose>
	
	<a href="/PodwiezMnie/">Powrót do strony głównej</a>
</body>
</html>