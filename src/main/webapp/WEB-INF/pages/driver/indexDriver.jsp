<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Zarządzanie kierowcami</title>
</head>
<body>
	<h1>Zarządzanie kierowcami</h1>
	Wybierz akcję do wykonania:<br />
	<a href="/PodwiezMnie/driver/add">Dodaj kierowcę</a><br />
	<a href="/PodwiezMnie/driver/edit">Zmień dane kierowcy</a><br />
	<a href="/PodwiezMnie/driver/delete">Usuń kierowcę z bazy</a><br />
	<a href="/PodwiezMnie/driver/showAll">Pokaż kierowców w bazie</a><br />
	<br />
	<a href="/PodwiezMnie">Powrót</a>
</body>
</html>