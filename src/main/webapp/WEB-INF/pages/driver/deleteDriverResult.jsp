<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Usuwanie kierowcy</title>
</head>
<body>
	<h1 id="greeting">Usunięto kierowcę</h1>
	
	<form:form commandName="driver">
		<c:choose>
			<c:when test="${!empty driverList }">
				<table class="data">
					<tr>
						<th>Imię</th>
						<th>Nazwisko</th>
						<th>Email</th>
					</tr>
					<c:forEach items="${driverList}" var="driver">
						<tr>
							<td>${driver.firstName }</td>
							<td>${driver.lastName }</td>
							<td>${driver.email }</td>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:otherwise>
				Nie wskazano kierowców do usunięcia.<br />
			</c:otherwise>
		</c:choose>
		</form:form>
	<a href="/PodwiezMnie/driver">Powrót</a>
</body>
</html>