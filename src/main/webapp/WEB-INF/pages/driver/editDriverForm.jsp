<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Edycja kierowcy</title>
</head>
<body>
	<h1 id="greeting">Edycja kierowcy</h1>
	
	<form:form commandName="driver">
			<table>
			<tr>
				<td>Imię</td>
				<td><form:input path="firstName" /></td>
				<td><form:errors path="firstName"/></td>
			</tr>
			<tr>
				<td>Nazwisko</td>
				<td><form:input path="lastName" /></td>
				<td><form:errors path="lastName"/></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><form:input path="email"/></td>
				<td><form:errors path="email"/></td>
			</tr>
			<tr>
				<td>Przypisane samochody</td>
				<td>
					<c:forEach var="cars" items="${driver.cars }">
						${cars.brand }<br />
					</c:forEach>
				
				</td>			
			</tr>
			<tr>
				<td><input type="submit" value="Zapisz"/>
			</tr>
			</table>                
        </form:form>
        ${info}<br />
        <a href="/PodwiezMnie/driver/edit">Powrót do wyboru kierowcy.</a>
        
</body>
</html>