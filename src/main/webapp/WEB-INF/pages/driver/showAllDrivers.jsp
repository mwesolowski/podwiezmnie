<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Kierowcy w bazie</title>
</head>
<body>
	<h1 id="greeting">Lista kierowców w bazie</h1>
	<c:choose>
		<c:when test="${!empty drivers }">
			<table class="data">
				<tr>
					<th>Imię</th>
					<th>Nazwisko</th>
					<th>Email</th>
					<th>Przypisane samochody</th>
				</tr>
				<c:forEach items="${drivers}" var="driver">
					
					<tr>
						<td>${driver.firstName }</td>
						<td>${driver.lastName }</td>
						<td>${driver.email }</td>
						<td>				
							<c:forEach items="${driver.cars }" var="car">
								${car.brand }<br />
							</c:forEach>
						</td>
					</tr>
					</c:forEach>
				
			</table>
		</c:when>
		<c:otherwise>
			Brak kierowców w bazie.<br />
		</c:otherwise>
	</c:choose>
	
	<a href="/PodwiezMnie/driver">Powrót</a>
</body>
</html>