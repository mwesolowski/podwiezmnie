<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wybór kierowcy do edycji</title>
</head>
<body>
	<h1 id="greeting">Wybierz kierowcę do edycji:</h1>
	<c:choose>
		<c:when test="${!empty driverList }">
			<table class="data">
				<tr>
					<th>Imię</th>
					<th>Nazwisko</th>
					<th>Email</th>
					<th>Akcja</th>
				</tr>
				<c:forEach items="${driverList}" var="driver">
					<tr>
						<td>${driver.firstName }</td>
						<td>${driver.lastName }</td>
						<td>${driver.email }</td>
						<td><a href="edit/${driver.id }">Edytuj</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
			Brak kierowców w bazie.<br />
		</c:otherwise>
	</c:choose>
	<a href="/PodwiezMnie/driver">Powrót</a>
</body>
</html>