<%@ page language="java" pageEncoding="UTF-8" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Wybór kierowcy do usunięcia</title>
</head>
<body>
	<h1 id="greeting">Wybierz kierowcę do usunięcia:</h1>
	<form:form commandName="driver">
		<c:choose>
			<c:when test="${!empty driverList }">
				<table class="data">
					<tr>
						<th>Imię</th>
						<th>Nazwisko</th>
						<th>Email</th>
						<th>Usuń</th>
					</tr>
					<c:forEach items="${driverList}" var="driver">
						<tr>
							<td>${driver.firstName }</td>
							<td>${driver.lastName }</td>
							<td>${driver.email }</td>
							<td><form:checkbox path="id" value="${driver.id }" /></td>
						</tr>
					</c:forEach>
					<tr>
						<td><input type="submit" value="Wyślij"></td>
					</tr>
				</table>
			</c:when>
			<c:otherwise>
				Brak kierowców w bazie.<br />
			</c:otherwise>
		</c:choose>
		</form:form>
	<a href="/PodwiezMnie/driver">Powrót</a>
</body>
</html>